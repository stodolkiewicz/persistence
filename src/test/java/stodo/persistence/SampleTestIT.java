package stodo.persistence;

import stodo.persistence.model.Post;
import stodo.persistence.repository.PostRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.List;

public class SampleTestIT extends AbstractPostgresIT {

    @Autowired
    PostRepository postRepo;

    @Test
    public void test1() {
        Post post = new Post();
        post.setText("brand new post!!!");

        postRepo.save(post);
        postRepo.flush();

        List<Post> all = postRepo.findAll();

        System.out.println(all);
    }

    @Test
    public void test2() {
        List<Post> all = postRepo.findAll();
        System.out.println(all);
    }

}
