package stodo.profiles;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles({"properties-testing", "default"})
public class TestProperties {

    @Value("${first.property}")
    String firstProperty;

    @Value("${some-random-property.value}")
    String someRandomProperty;

    @Value("${some-random-property.second-value}")
    String someRandomPropertySecondValue;

    @Autowired
    ConfigProperties configProperties;

    private static final String FIRST_PROPERTY_VALUE = "a";
    private static final String SOME_RANDOM_PROPERTY_VALUE = "value from application-properties-testing.properties";
    private static final String SOME_RANDOM_PROPERTY_SECOND_VALUE = "value from src/main/resources/application.yml is accessible";

    @Test
    void valuesFromTestPropertiesAreAccessible(){
        assertEquals(FIRST_PROPERTY_VALUE, firstProperty);
    }

    @Test
    void someRandomPropertyValueOverridesDefaultProfileValue(){
        assertEquals(SOME_RANDOM_PROPERTY_VALUE, someRandomProperty);
    }

    @Test
    void defaultProfileValuesAreAccessibleIfNotOverriddenByTestProperties() {
        assertEquals(SOME_RANDOM_PROPERTY_SECOND_VALUE, someRandomPropertySecondValue);
    }

    @Test
    void configurationPropertiesAnnotationWorks () {
        String firstProperty = configProperties.getProperty();
        String otherProperty = configProperties.getOtherProperty();

        assertEquals(FIRST_PROPERTY_VALUE, firstProperty);
        assertEquals("b", otherProperty);
    }

}
