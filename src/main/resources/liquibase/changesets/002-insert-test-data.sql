--liquibase formatted sql

--changeset stodo:insert-test-data-into-user
INSERT INTO APP_USER(ID, NAME) VALUES(NEXTVAL('app_user_id_sequence'), 'Jason');
INSERT INTO APP_USER(ID, NAME) VALUES(NEXTVAL('app_user_id_sequence'), 'Andrew');
--rollback DELETE FROM TABLE POST

--changeset stodo:insert-test-data-into-post
INSERT INTO POST(ID, TITLE, TEXT) VALUES(NEXTVAL('post_id_seq'), 'post1 title', 'I am posting now');
INSERT INTO POST(ID, TITLE, TEXT) VALUES(NEXTVAL('post_id_seq'), 'post2 title', 'post2 text');
INSERT INTO POST(ID, TITLE, TEXT) VALUES(NEXTVAL('post_id_seq'), 'post3 title', 'post3 text');
INSERT INTO POST(ID, TITLE, TEXT) VALUES(NEXTVAL('post_id_seq'), 'post4 title', 'post3 text');
--rollback DELETE FROM TABLE POST

--changeset stodo:insert-test-data-into-post_details
INSERT INTO POST_DETAILS(POST_ID, CREATED_BY, CREATED_ON) VALUES( (select id from post where title = 'post1 title'), 'Dawid', now());
INSERT INTO POST_DETAILS(POST_ID, CREATED_BY, CREATED_ON) VALUES( (select id from post where title = 'post2 title'), 'Dawid', now());
INSERT INTO POST_DETAILS(POST_ID, CREATED_BY, CREATED_ON) VALUES( (select id from post where title = 'post3 title'), 'Dawid', now());
INSERT INTO POST_DETAILS(POST_ID, CREATED_BY, CREATED_ON) VALUES( (select id from post where title = 'post4 title'), 'Dawid', now());
--rollback DELETE FROM TABLE POST_DETAILS

--changeset stodo:insert-test-data-into-post_comment
INSERT INTO POST_COMMENT(ID, POST_ID, REVIEW, APP_USER_ID) VALUES(NEXTVAL('post_comment_id_seq'), (select id from post where title = 'post1 title'), 'post1 is good', 1);
INSERT INTO POST_COMMENT(ID, POST_ID, REVIEW, APP_USER_ID) VALUES(NEXTVAL('post_comment_id_seq'), (select id from post where title = 'post1 title'), 'post1 is NOT good!', 2);
INSERT INTO POST_COMMENT(ID, POST_ID, REVIEW, APP_USER_ID) VALUES(NEXTVAL('post_comment_id_seq'), (select id from post where title = 'post2 title'), 'post2 comment', 1);
INSERT INTO POST_COMMENT(ID, POST_ID, REVIEW) VALUES(NEXTVAL('post_comment_id_seq'), (select id from post where title = 'post4 title'), 'post4 comment');
--rollback DELETE FROM TABLE POST_COMMENT

--changeset stodo:insert-test-data-into-tag
INSERT INTO TAG(ID, NAME) VALUES(NEXTVAL('tag_id_seq'), 'quality content');
INSERT INTO TAG(ID, NAME) VALUES(NEXTVAL('tag_id_seq'), 'buyItNow');
INSERT INTO TAG(ID, NAME) VALUES(NEXTVAL('tag_id_seq'), 'That''s what she said');
INSERT INTO TAG(ID, NAME) VALUES(NEXTVAL('tag_id_seq'), 'PlsDeleteAccount');
--rollback DELETE FROM TABLE TAG

--changeset stodo:insert-test-data-into-post_tag
INSERT INTO POST_TAG(POST_ID, TAG_ID) VALUES(10,1);
INSERT INTO POST_TAG(POST_ID, TAG_ID) VALUES(10,2);
INSERT INTO POST_TAG(POST_ID, TAG_ID) VALUES(20,4);
INSERT INTO POST_TAG(POST_ID, TAG_ID) VALUES(30,1);
INSERT INTO POST_TAG(POST_ID, TAG_ID) VALUES(40,1);
INSERT INTO POST_TAG(POST_ID, TAG_ID) VALUES(40,2);
INSERT INTO POST_TAG(POST_ID, TAG_ID) VALUES(40,3);
INSERT INTO POST_TAG(POST_ID, TAG_ID) VALUES(40,4);
--rollback DELETE FROM TABLE POST_TAG