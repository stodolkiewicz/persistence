package stodo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@EnableCaching
@EnableJpaRepositories("stodo.persistence.repository")
@SpringBootApplication
public class PersistenceApplication {

	public static void main(String[] args) {
		SpringApplication.run(PersistenceApplication.class, args);
	}

	// todo:

	// MAPSTRUCT should map Post to - > PostWithCommentsDTO (with nested AppUserDTO!)

    // HIBERNATE VALIDATOR WITH CUSTOM ANNOTATIONS TO VALIDATE POST BODY

	// MAPSTRUCT - https://cupofcodes.pl/mapstruct-czyli-jak-szybko-i-wygodnie-mapowac-obiekty-cz-1/

	// mockMvc tests from https://reflectoring.io/spring-boot-paging/

	// todo FIRST
	// EXCEPTION HANDLING IN SPRING + BEST PRACTICES. @ControllerAdvice, @ExceptionHandler, CustomException, ResponseEntity

	// scrollable resultset https://www.baeldung.com/hibernate-pagination

    // entity graph from nullpointer

	// LOCK MODES

    // read on ArgumentCaptor

	// DoInJPA

	// add gitlab.yml and run unit tests

}
