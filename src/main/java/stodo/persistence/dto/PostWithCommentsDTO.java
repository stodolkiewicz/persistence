package stodo.persistence.dto;

import lombok.Data;
import java.util.List;

@Data
public class PostWithCommentsDTO {
    private Long id;
    private String title;
    private String text;
    private List<PostCommentDTO> comments;
}
