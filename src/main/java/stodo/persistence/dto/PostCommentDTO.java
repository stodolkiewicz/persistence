package stodo.persistence.dto;

import lombok.Data;

@Data
public class PostCommentDTO {
    private Long id;
    private String review;
    private AppUserDTO appUserDTO;
}
