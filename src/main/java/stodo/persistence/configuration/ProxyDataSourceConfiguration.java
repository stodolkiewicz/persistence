package stodo.persistence.configuration;

import net.ttddyy.dsproxy.listener.logging.SLF4JLogLevel;
import net.ttddyy.dsproxy.support.ProxyDataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.context.annotation.Profile;

import javax.sql.DataSource;

@Configuration
public class ProxyDataSourceConfiguration {

    @Bean
    @Primary
    @Profile("postgres")
    public DataSource dataSource(DataSource actualDataSource) {
        return ProxyDataSourceBuilder
                .create(actualDataSource)
                .name("MyOwnDataSourceProxyName")
                .logQueryBySlf4j(SLF4JLogLevel.INFO)
                .multiline()
                .build();
    }
}
