package stodo.persistence.repository;

import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Repository;
import stodo.persistence.model.Post;

import javax.persistence.EntityGraph;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class CustomPostRepositoryImpl implements CustomPostRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public Long saveMyPost(Post post) {

        Post merge = em.merge(post);

        return merge.getId();
    }

    //    Pagination With EntityGraph -> getting rid of n + 1
    public List<Post> myFindAllPaginated(PageRequest pageRequest) {
        int pageNumber = pageRequest.getPageNumber();
        int pageSize = pageRequest.getPageSize();

        EntityGraph<Post> entityGraph = em.createEntityGraph(Post.class);
        entityGraph.addAttributeNodes("details");
        entityGraph.addSubgraph("comments")
                .addAttributeNodes("user");

        TypedQuery<Post> query = em.createQuery("from Post", Post.class)
                .setFirstResult(pageNumber)
                .setMaxResults(pageSize)
                .setHint("javax.persistence.fetchgraph", entityGraph);

        List<Post> posts = query.getResultList();

        return posts;
    }
}
