package stodo.persistence.repository;

import org.springframework.data.domain.PageRequest;
import stodo.persistence.model.Post;

import java.util.List;

public interface CustomPostRepository {
    Long saveMyPost(Post post);
    List<Post> myFindAllPaginated(PageRequest pageRequest);
}
