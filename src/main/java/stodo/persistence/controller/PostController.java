package stodo.persistence.controller;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import stodo.persistence.dto.PostDTO;
import stodo.persistence.dto.PostWithCommentsDTO;
import stodo.persistence.model.Post;
import stodo.persistence.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import stodo.persistence.service.PostService;

import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/post")
public class PostController {

    @Autowired
    PostRepository postRepository;

    @Autowired
    PostService postService;

    @GetMapping("/{postId}")
    public PostDTO getPostById(@PathVariable Long postId) {
        return postService.getSinglePost(postId);
    }

    @PutMapping()
    public PostDTO editPost(@RequestBody Post post) {
        return postService.editPost(post);
    }

    @DeleteMapping("/{postId}")
    public void deletePost(@PathVariable Long postId) {
        postService.deletePostById(postId);
    }

    @GetMapping("/paged-posts-with-comments")
    public List<PostWithCommentsDTO> getPagedPostsWithComments(
            @RequestParam Optional<Integer> page,
            @RequestParam Optional<Integer> size,
            @RequestParam Optional<String> sortBy
    ) {
        return postService.getPostsWithComments(
                PageRequest.of(
                        page.orElse(0),
                        size.orElse(20),
                        Sort.by(Sort.Direction.ASC, sortBy.orElse("id"))
                )
        );
    }

    @PostMapping
    @Transactional
    public void testBatchInserts(@RequestBody Integer numberOfPosts) {
        for(int i = 0; i < numberOfPosts; i++) {
            postRepository.save(new Post("generatedTitle" + i, "generatedText" + i));
        }
    }

    @PostMapping("/save")
    public void savePost(@RequestBody Post post) {
        postRepository.saveMyPost(post);
    }

}
