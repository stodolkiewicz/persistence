package stodo.persistence.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import stodo.persistence.dto.PostDTO;
import stodo.persistence.dto.PostWithCommentsDTO;
import stodo.persistence.model.Post;

@Mapper
public interface PostMapper {
    PostMapper INSTANCE = Mappers.getMapper( PostMapper.class );

    @Mapping(source = "comments", target = "comments")
    PostWithCommentsDTO mapToPostWithCommentsDTO(Post post);

    PostDTO mapToPostDTO(Post post);
}
