package stodo.persistence.mapper;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;
import stodo.persistence.dto.AppUserDTO;
import stodo.persistence.dto.PostWithCommentsDTO;
import stodo.persistence.model.AppUser;
import stodo.persistence.model.Post;

@Mapper
public interface AppUserMapper {
    AppUserMapper INSTANCE = Mappers.getMapper( AppUserMapper.class );

    AppUserDTO mapToAppUserDTO(AppUser AppUser);
}
