package stodo.persistence.service;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import stodo.persistence.dto.PostDTO;
import stodo.persistence.dto.PostWithCommentsDTO;
import stodo.persistence.mapper.PostMapper;
import stodo.persistence.model.Post;
import stodo.persistence.repository.PostCommentRepository;
import stodo.persistence.repository.PostRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
@Transactional
public class PostService {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    PostRepository postRepository;

    @Autowired
    PostCommentRepository postCommentRepository;

    PostMapper postMapper = Mappers.getMapper(PostMapper.class);

    @Cacheable(cacheNames = "singlePost", key = "#id")
    public PostDTO getSinglePost(Long id) {
        Post post = postRepository.findById(id).orElseThrow(EntityNotFoundException::new);

        return postMapper.mapToPostDTO(post);
    }

    @CachePut(cacheNames = "singlePost", key = "#result.id")
    public PostDTO editPost(Post post) {
        Post retrievedPost = postRepository.findById(post.getId()).orElseThrow(EntityNotFoundException::new);
        retrievedPost.setText(post.getText());

        PostDTO postDTO = Mappers.getMapper(PostMapper.class).mapToPostDTO(retrievedPost);

        return postDTO;
    }

    @CacheEvict(cacheNames = "singlePost")
    public void deletePostById(Long postId) {
        postRepository.deleteById(postId);
    }

    @Cacheable(cacheNames = "getPostsWithComments")
    public List<PostWithCommentsDTO> getPostsWithComments(PageRequest pageRequest) {
        List<Post> posts = postRepository.myFindAllPaginated(pageRequest);

        PostMapper postMapper = Mappers.getMapper(PostMapper.class);

        return posts.stream().map(postMapper::mapToPostWithCommentsDTO).collect(toList());
    }

}
