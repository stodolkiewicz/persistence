package stodo.persistence.service;

import org.mapstruct.factory.Mappers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import stodo.persistence.dto.PostWithCommentsDTO;
import stodo.persistence.mapper.PostMapper;
import stodo.persistence.model.Post;
import stodo.persistence.model.Tag;
import stodo.persistence.repository.TagRepository;

import java.util.List;

@Service
public class TagService {

    @Autowired
    TagRepository tagRepository;

    @Cacheable(cacheNames = "findAllTags")
    public List<Tag> findAllTags() {
        List<Tag> tags = tagRepository.findAll();

        return tags;
    }

}
